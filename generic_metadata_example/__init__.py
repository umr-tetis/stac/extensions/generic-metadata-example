from .my_extension import MyExtension, MyExtensionModel

# Remember to bump version before each new package build
__version__ = "0.0.10"