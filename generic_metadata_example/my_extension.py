from stac_extension_genmeta import create_extension_cls
from pydantic import BaseModel, Field, ConfigDict
from typing import List

SCHEMA_URI: str = (
    "https://forgemia.inra.fr/umr-tetis/stac/extensions/"
    "generic-metadata-example/-/raw/main/json_schema/v1.0.0/schema.json"
)


# Extension model
class MyExtensionModel(BaseModel):
    # Required so that one model can be instantiated with the attribute name
    # rather than the alias
    model_config = ConfigDict(populate_by_name=True)

    # Metadata fields
    name: str = Field(title="Process name", alias="prefix:name")
    authors: List[str] = Field(title="Authors", alias="prefix:authors")
    version: str = Field(title="Process version", alias="prefix:version")


# Extension class
MyExtension = create_extension_cls(
    model_cls=MyExtensionModel,
    schema_uri=SCHEMA_URI
)
