from generic_metadata_example import MyExtension, MyExtensionModel
from stac_extension_genmeta.testing import basic_test, is_schema_url_synced

ext_md = MyExtensionModel(
    name="test",
    authors=["michel", "denis"],
    version="alpha"
)

# Basic tests (apply the extension to item/asset, read metadata from
# extended item/asset, etc.)
basic_test(ext_md, MyExtension)

# Check that the online schema is in sync with the actual schema of the class
is_schema_url_synced(MyExtension)
