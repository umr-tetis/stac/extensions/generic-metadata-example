# Description

This is an example how to use `stac-extension-genmeta`.

# Installation

Python package is hosted in gitlab package registry.
You need to show pip the extra indexes:

```commandline
pip install generic-metadata-example \
--extra-index-url https://forgemia.inra.fr/api/v4/projects/10919/packages/pypi/simple \
--extra-index-url https://forgemia.inra.fr/api/v4/projects/10920/packages/pypi/simple
```

Where:

- `https://forgemia.inra.fr/api/v4/projects/10919/packages/pypi/simple` is for the `stac-extension-genmeta` package
- `https://forgemia.inra.fr/api/v4/projects/10920/packages/pypi/simple` is for the `generic-metadata-example` package

# Usage

## Apply extension to a STAC object

```python
from generic_metadata_example import MyExtension, MyExtensionModel

ext_md = MyExtensionModel(
    name="test",
    authors=["michel", "denis"],
    version="alpha"
)

stac_item_or_asset = ...
ext = MyExtension.ext(stac_item_or_asset, add_if_missing=True)
ext.apply(ext_md)
```

## Read extension metadata from a STAC object

```python
from generic_metadata_example import MyExtension

stac_item_or_asset = ...
ext = MyExtension(stac_item_or_asset)
print(ext.name)
print(ext.version)
print(ext.authors)
```

# Developer's checklist

1. Implement your own custom extension
2. Push the code on gitlab and copy the schema generated at the 1st CI job, 
or generate the json schema using `MyExtension.export_schema()`
4. Put the json schema in `json_schema/v1.0.0/schema.json` of the repository
5. Push the code, check pipeline status
6. Tag to publish the pip package

As explained in the *installation* section, you will need to note the pip index 
of gitlab.
